#include <iostream>
#include <memory>

import renderer;
import vec3;
import viewport;
import sphere;
import hittable;


const auto aspect_ratio = 16.0 / 9.0;


// Camera
const auto viewport_height = 2.0;
const auto viewport_width = aspect_ratio * viewport_height;
const auto focal_length = 1.0;
const auto origin = vec3(0, 0, 0);
const auto horizontal = vec3(viewport_width, 0, 0);
const auto vertical = vec3(0, viewport_height, 0);
const auto lower_left_corner = origin - horizontal/2 - vertical/2 - vec3(0, 0, focal_length);

viewport camera {
    viewport_height,
    viewport_width,
    focal_length,
    origin,
    horizontal,
    vertical,
    lower_left_corner
};

// Image
const int image_width = 400;
const int image_height = static_cast<int>(image_width / aspect_ratio);

hittable_list world;

renderer output {image_width, image_height, 255, camera};

int main() {
    world.add(std::make_shared<sphere>(point3(0,0,-1), 0.5));
    world.add(std::make_shared<sphere>(point3(0,-100.5,-1), 100));
    output.render(std::cout, world);
    std::cerr << "done" << std::endl;
    return 0;
}
