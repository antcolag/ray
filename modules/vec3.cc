module;

#include <cmath>
#include <ostream>

export module vec3;

export struct vec3 {
    double x;
    double y;
    double z;

    double & r = x;
    double & g = y;
    double & b = z;

    vec3() : vec3{0,0,0} {}

    vec3(double x, double y, double z) : x{x}, y{y}, z{z} {}

    vec3 & moveTo(const vec3 && o) {
        this->x = o.x;
        this->y = o.y;
        this->z = o.z;
        return *this;
    }

    vec3 operator-() const {
        return vec3(-x, -y, -z);
    }

    vec3& operator+=(const vec3 &v) {
        x += v.x;
        y += v.y;
        z += v.z;
        return *this;
    }

    vec3& operator*=(const double t) {
        x *= t;
        y *= t;
        z *= t;
        return *this;
    }

    vec3& operator/=(const double t) {
        return *this *= 1/t;
    }

    double length() const {
        return std::sqrt(length_squared());
    }

    double length_squared() const {
        return x * x + y * y + z * z;
    }
};

// Type aliases for vec3
export using point3 = vec3;   // 3D point
export using color = vec3;    // RGB color

// vec3 Utility Functions

export inline std::ostream& operator<<(std::ostream &out, const vec3 &v) {
    return out << v.x << ' ' << v.y << ' ' << v.z;
}

export inline vec3 operator+(const vec3 &u, const vec3 &v) {
    return vec3(u.x + v.x, u.y + v.y, u.z + v.z);
}

export inline vec3 operator-(const vec3 &u, const vec3 &v) {
    return vec3(u.x - v.x, u.y - v.y, u.z - v.z);
}

export inline vec3 operator*(const vec3 &u, const vec3 &v) {
    return vec3(u.x * v.x, u.y * v.y, u.z * v.z);
}

export inline vec3 operator*(double t, const vec3 &v) {
    return vec3(t * v.x, t * v.y, t * v.z);
}

export inline vec3 operator*(const vec3 &v, double t) {
    return t * v;
}

export inline vec3 operator/(vec3 v, double t) {
    return (1/t) * v;
}

export inline double dot(const vec3 &u, const vec3 &v) {
    return u.x * v.x
        + u.y * v.y
        + u.z * v.z;
}

export inline vec3 cross(const vec3 &u, const vec3 &v) {
    return vec3(u.y * v.z - u.z * v.y,
                u.z * v.x - u.x * v.z,
                u.x * v.y - u.y * v.x);
}

export inline vec3 unit_vector(vec3 v) {
    return v / v.length();
}
