module;

#include <ostream>

export module color;


import vec3;


export void write_color(std::ostream &out, vec3 pixel_color, const int depth) {
    // Write the translated [0,255] value of each color component.
    out << static_cast<int>(depth * pixel_color.r) << ' '
        << static_cast<int>(depth * pixel_color.g) << ' '
        << static_cast<int>(depth * pixel_color.b) << std::endl;
}