module;

export module viewport;

export import vec3;

export struct viewport {
    double height;
    double width;
    double focal_length;
    vec3 origin;
    vec3 horizontal;
    vec3 vertical;
    vec3 lower_left_corner;
};
