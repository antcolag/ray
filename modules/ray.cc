module;


export module ray;

export import vec3;

export struct ray {
    ray() {}
    ray(const vec3& origin, const vec3& direction)
        : origin(origin), direction(direction)
    {}

    vec3 at(double t) const {
        return origin + t * direction;
    }

    vec3 origin;
    vec3 direction;
};