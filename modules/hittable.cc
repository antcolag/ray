module;

#include <utility>
#include <memory>
#include <vector>

export module hittable;

export import ray;
export import vec3;

export struct hit_record {
    vec3 p;
    vec3 normal;
    double t;
    bool front_face;

    inline void set_face_normal(const ray& r, const vec3& outward_normal) {
        front_face = dot(r.direction, outward_normal) < 0;
        normal.moveTo(std::move(front_face ? outward_normal : -outward_normal));
    }

    hit_record& operator=(const hit_record& o) {
        this->p.moveTo(std::move(o.p));
        this->normal.moveTo(std::move(o.normal));
        this->t = o.t;
        this->front_face = o.front_face;
        return *this;
    }
};

export struct hittable {
    virtual bool hit(
        const ray& r,
        double t_min,
        double t_max,
        hit_record& rec
    ) const {
        return false;
    };
};

export struct hittable_list : hittable {
    hittable_list() {}
    hittable_list(std::shared_ptr<hittable> object) { add(object); }

    void clear() { objects.clear(); }
    void add(std::shared_ptr<hittable> object) { objects.push_back(object); }

    virtual bool hit(const ray& r, double t_min, double t_max, hit_record& rec) const override
    {
        hit_record temp_rec;
        bool hit_anything = false;
        auto closest_so_far = t_max;

        for (const auto& object : objects) {
            if (object->hit(r, t_min, closest_so_far, temp_rec)) {
                hit_anything = true;
                closest_so_far = temp_rec.t;
                rec = temp_rec;
            }
        }

        return hit_anything;
    }

    std::vector<std::shared_ptr<hittable>> objects;
};

