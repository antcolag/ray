module;

#include <ostream>
#include <limits>
#include <cmath>

export module renderer;

export import ray;
export import vec3;
export import color;
export import viewport;
export import hittable;

export double hit_sphere(const vec3& center, double radius, const ray& r) {
    vec3 oc = r.origin - center;
    auto a = dot(r.direction, r.direction);
    auto half_b = dot(oc, r.direction);
    auto c = oc.length_squared() - radius * radius;
    auto discriminant = half_b * half_b - a * c;
    if (discriminant < 0) {
        return -1.0;
    } else {
        return (-half_b - std::sqrt(discriminant) ) / a;
    }
}

export color ray_color(const ray& r, const hittable & world) {
    hit_record rec;
    if (world.hit(r, 0, std::numeric_limits<double>::max(), rec)) {
        return 0.5 * (rec.normal + color(1,1,1));
    }

    vec3 unit_direction = unit_vector(r.direction);
    auto t = 0.5*(unit_direction.y + 1.0);
    return (1.0 - t) * color(1.0, 1.0, 1.0) + t * color(0.5, 0.7, 1.0);
}

export struct renderer {
    const int width;
    const int height;
    const int depth;
    viewport & camera;
    renderer(
        const int width,
        const int height,
        const int depth,
        viewport & camera
    ) : width {width},
        height {height},
        depth {depth},
        camera {camera}
    {};

    void render(std::ostream & output, const hittable & world) {
        output  << "P3" << std::endl                   // print image format
                << width << ' ' << height << std::endl // print image size
                << depth << std::endl;                 // print color depth

        for (int j{height-1}; j >= 0; --j) {
            for (int i = 0; i < width; ++i) {
                auto u = static_cast<double>(i) / (width-1);
                auto v = static_cast<double>(j) / (height-1);
                vec3 direction = camera.lower_left_corner + 
                                    u * camera.horizontal + 
                                    v * camera.vertical -
                                    camera.origin;

                ray r(camera.origin, direction);

                write_color(output, ray_color(r, world), depth);
            }
        }
    }
};