# Ray tracing in a weekend by asdru

My experiments with the book [_Ray Tracing in One Weekend_](https://raytracing.github.io/books/RayTracingInOneWeekend.html).

I decided to use C++ modules even if my text editor is not supporting them. I'm a fool.

## Download latest release

You can download the latest release from [here](https://gitlab.com/antcolag/ray/-/releases).

## How to build

There are several ways to build the project

### With Makefile

This method requires make and g++ at least at version 11

```
make
```

### With Docker

If you don't want to install make and g++ you can use Docker to build the program.

```
docker run --rm --volume=$(pwd):/prog --workdir=/prog -it gcc make
```

## Build and debug with vscode

The configuration in .vscode allows to build and debug the app using vscode.

You can run the *`Build`* tasks using `Ctrl-Shift-B` and you can debug using the integrated debugger in vscode.

### Requirements
In order to build the app using vscode you need `make` and `g++` at least at
version 11 and also you need to install the [cpptools-extension-pack vscode extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools-extension-pack).
