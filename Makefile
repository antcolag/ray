CXX := g++
CXXFLAGS := -g -O0 --std=c++2a -fmodules-ts
modules := vec3 ray viewport hittable color sphere renderer
objects  := $(foreach module, $(modules), modules/$(module).o)
output := main
img := img.ppm

print ::
	@echo "modules: $(modules)";


build :: $(objects)
	$(CXX) $(CXXFLAGS) $(objects) main.cc -o $(output)

clean ::
	rm -rf gcm.cache $(img) $(objects)

clean-full :: clean
	rm -f $(output)

$(output) : print build clean
	@echo 'done!'

display :: build
	./$(output) | display

.DEFAULT_GOAL := $(output)
.PHONY        := print build clean clean-full $(output) display